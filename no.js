var http = require('http');
var url = require('url');
var fs = require('fs');
var querystring = require('querystring');
var MongoClient = require('mongodb').MongoClient;
var isrod = "mongodb://rod:rod@10.191.237.56:27017/admin";
var mime =
{
  'html':'text/html',
  'css':'text/css',
  'jpg':'image/jpg',
  'ico':'image/x-icon',
  'mp3':'audio/mpeg3',
  'mp4':'video/mp4'
}
var serv = http.createServer(function(req,res)
{
   var ob=url.parse(req.url);
   var wa=ob.pathname;
   if(wa=='/')
       wa='/index.html';
   onw(req,res,wa);
});
serv.listen(80);
function onw(req,res,wa)
{
    console.log(wa);
    switch (wa)
    {
       case '/dale':
       {
          reco(req,res);
          break;
       }
       case '/thing':
       {
          maybe(req,res);
          break;
       }
       case '/those':
       {
          notch(req,res);
          break;
       }
       default :
       {
          fs.exists(wa,function(existe)
          {
             if(existe)
             {
                fs.readFile(wa,function(err,cont)
                {
                   if (err)
                   {
                      res.writeHead(500,{'Content-Type':'text/plain'});
                      res.write('Fallo');
                      res.end();
                   }
                   else
                   {
                      var vec = wa.split('.');
                      var ext=vec[vec.length-1];
                      var mimearc=mime[ext];
                      res.writeHead(200,{'Content-Type':mimearc});
                      res.write(cont);
                      res.end();
                   }
                });
             }
             else
             {
                res.writeHead(404, {'Content-Type':'text/html'});
                res.write('<!doctype html><html><head></head><body>Recurso inexistente</body></html>');
                res.end();
             }
          });
       }
    }
}
function maybe(req,res)
{
   var info='';
   req.on('data',function(thing)
   {
      info += thing;
   });
   req.on('end',function()
   {
     var form=querystring.parse(info);
     var nom=form['nombre'];
     var cam=form['campo'];
     var my='{';
     my +='\"';
     my +=nom;
     my +='\"';
     my +=':';
     my +='\"';
     my +=cam;
     my +='\"';
     my +='}';
     var objmy = JSON.parse(my);
     MongoClient.connect(isrod,{useNewUrlParser:true},function(err,db)
      {
        if(err) throw err;
        var dbo=db.db("some");
        dbo.collection("so").deleteOne(objmy,function(err,foo)
        {
          if(err) throw err;
          db.close();
        });
      });
   });
}
function notch(req,res)
{
   var info='';
   req.on('data',function(those)
   {
      info += those;
   });
   req.on('end',function()
   {
     var form=querystring.parse(info);
     var nom=form['nombre'];
     var cam=form['campo'];
     if(nom != "")
     {
       var my='{';
       my +='\"';
       my +=nom;
       my +='\"';
       my +=':';
       my +='\"';
       my +=cam;
       my +='\"';
       my +='}';
       var objmy = JSON.parse(my);
     }
     MongoClient.connect(isrod,{useNewUrlParser:true},function(err,db)
      {
        if(err) throw err;
        var dbo=db.db("some");
        dbo.collection("so").find(objmy).toArray(function(err,foo)
        {
          if(err) throw err;
          var ko=JSON.stringify(foo, null, 2);
          res.writeHead(200,{'Content-Type':'text/javascript'});
          res.write(ko);
          res.end();
          db.close();
        });
      });
   });
}
function reco(req,res)
{
   var info='';
   req.on('data',function(dale)
   {
      info += dale;
   });
   req.on('end',function()
   {
      var form=querystring.parse(info);
      var nom=form['nombre'].split(',');
      var cam=form['campo'].split(',');
      var my='{';
      while(nom.length > 0)
      {
         my +='\"';
         my +=nom.shift();
         my +='\"';
         my +=':';
         my +='\"';
         my +=cam.shift();
         my +='\"';
         if(nom.length > 0)
         my +=",";
      }
      my +='}';
      var objmy = JSON.parse(my);
      MongoClient.connect(isrod,{useNewUrlParser:true},function(err,db)
      {
        if(err) throw err;
        var dbo=db.db("some");
        dbo.collection("so").insertOne(objmy,function(err,foo)
        {
          if(err) throw err;
          db.close();
        });
      });

   });
}
console.log('Up');